const express = require("express");
const router = express.Router();


const { paymentReq } = require("../controller/request")
const { response } = require("../controller/response")


router.post('/request', paymentReq )
router.post('/response', response)

module.exports = router
