var crypto = require('crypto');

exports.decrypt = function (encText, workingKey) {
    var m = crypto.createHash('md5');
    m.update(workingKey)
    var key = m.digest();
    var iv = '\x00\x01\x02\x03\x04\x05\x06\x07\x08\x09\x0a\x0b\x0c\x0d\x0e\x0f';	
    // const iv = crypto.randomBytes(32);	

    var decipher = crypto.createDecipheriv('aes-128-cbc', key, iv);
    var decoded = decipher.update(encText,'hex','utf8');
        decoded += decipher.final('utf8');
    return decoded;
}

exports.encrypt = function (plainText, workingKey) {
	
	var m = crypto.createHash('md5');
    	m.update(workingKey);
   	var key = m.digest();
	    var iv = '\x00\x01\x02\x03\x04\x05\x06\x07\x08\x09\x0a\x0b\x0c\x0d\x0e\x0f';
		//   const iv = crypto.randomBytes(32);	
	var cipher = crypto.createCipheriv('aes-128-cbc', key, iv);
	var encoded = cipher.update(plainText,'utf8','hex');
	encoded += cipher.final('hex');
	   	return encoded;
}