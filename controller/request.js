var ccav = require('../ccavutil');
    require('dotenv').config()

exports.paymentReq = function(request,response){
    
    let body = '',
        workingKey = process.env.WORKING_KEY,	//Put in the 32-Bit key shared by CCAvenues.
        accessCode = process.env.ACCESS_KEY,			//Put in the Access Code shared by CCAvenues.
        encRequest = '',
        formbody = '';

    
    try {
       
        request.on('data', function (data) {

            // console.log(data.toString().split(',').join('&'))
            body += data;
            encRequest = ccav.encrypt(body,workingKey); 
           
            // formbody = '<form id="nonseamless" method="post" name="redirect" action="https://secure.ccavenue.ae/transaction/transaction.do?command=initiateTransaction"/> <input type="hidden" id="encRequest" name="encRequest" value="' + encRequest + '"><input type="hidden" name="access_code" id="access_code" value="' + accessCode + '"><script language="javascript">document.redirect.submit();</script></form>';
            // 
        });
                        
        request.on('end', function () {
            // response.writeHeader(200, {"Content-Type": "text/html"});
            response.json(encRequest);
            response.end();
            });
            return; 

    } 
    
    catch (err) {
            
        console.log(err);
            res.status(400).json({
                err: err.message,
        });
    }
}
