const http = require('http'),
    fs = require('fs'),
    ccav = require('../ccavutil'),
    qs = require('querystring');
    require('dotenv').config()


exports.response = function(request,response){

        try {
            var ccavEncResponse='',
            ccavResponse='',	
            workingKey = process.env.WORKING_KEY,	//Put in the 32-Bit key shared by CCAvenues.
            ccavPOST = '';
        
            request.on('data', function (data) {
                ccavEncResponse += data;
                ccavPOST =  qs.parse(ccavEncResponse);
                var encryption = ccavPOST.encResp;
                ccavResponse = ccav.decrypt(encryption,workingKey);
                console.log("response",ccavResponse)
            });
    
            request.on('end', function () {
                let pData = '';
                pData = '<table border=1 cellspacing=2 cellpadding=2><tr><td>'	
                pData = pData + ccavResponse.replace(/=/gi,'</td><td>')
                pData = pData.replace(/&/gi,'</td></tr><tr><td>')
                pData = pData + '</td></tr></table>'
                
                htmlcode = '<html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"><title>Response Handler</title></head><body><center><font size="4" color="blue"><b>Response Page</b></font><br>'+ pData +'</center><br></body></html>';
                response.writeHeader(200, {"Content-Type": "text/html"});
                response.write(htmlcode);
               
                console.log(ccavResponse.split('&'))
                response.end();
            }); 	
        } catch (error) {
            console.log(err);
            response.status(400).json({
                err: err.message,
            });
        }

}
