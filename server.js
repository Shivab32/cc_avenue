const express = require('express');
const cors = require('cors')
const app = express()
require('dotenv').config()

const router = require('./routes/payment');


app.use(cors())
// app.use(express.json())
app.use(express.static('public'));
app.set('views', __dirname + '/Public');
app.engine('html', require('ejs').renderFile);


app.get('/', ( err, res) => {
    res.send("api hitting")
})

app.get('/about', function (req, res){
    res.render('form.html');
})

app.use('/api', router)

const port =  process.env.PORT || 3000

app.listen(port, (err, res) => {
    if (err) throw err
    console.log(`app is running on port ${port}`)
})